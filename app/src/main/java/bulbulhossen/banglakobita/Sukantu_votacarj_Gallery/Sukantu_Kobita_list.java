package bulbulhossen.banglakobita.Sukantu_votacarj_Gallery;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import bulbulhossen.banglakobita.Adapter_Gallery.CustomAdapter;
import bulbulhossen.banglakobita.Joshim_Uddin_Gallery.Joshim_1;
import bulbulhossen.banglakobita.R;


public class Sukantu_Kobita_list extends Fragment {
    Intent i;

    public Sukantu_Kobita_list() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Layout name change
        View rootView = inflater.inflate(R.layout.sukantu_kobita_list, container, false);
        //Layout name change

        //change code
        String[] str = getActivity().getResources().getStringArray(R.array.sukantu_votacarj_kobita_listName);
        //change code


        final CustomAdapter adapter = new CustomAdapter(getContext(), android.R.id.text1, str);

        //change code
        ListView listView = (ListView) rootView.findViewById(R.id.sukantu_listView);
        //change code
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Toast.makeText(getActivity(), "You Just Clicked:" + adapter.getItem(position), Toast.LENGTH_LONG);

                switch (position) {
                    case 0:

                        //Get fragment code
                        Fragment fragment_1 = new sukantu_vortacarj_1();

                        FragmentManager fragmentManager_1 = getFragmentManager();
                        FragmentTransaction fragmentTransaction_1 = fragmentManager_1.beginTransaction();
                        fragmentTransaction_1.replace(R.id.container_body, fragment_1);
                        fragmentTransaction_1.addToBackStack(null);
                        fragmentTransaction_1.commit();
                        break;

                    case 1:
                        //Get fragment code
                        Fragment fragment_2 = new sukantu_vortacarj_2();

                        FragmentManager fragmentManager_2 = getFragmentManager();
                        FragmentTransaction fragmentTransaction_2 = fragmentManager_2.beginTransaction();
                        fragmentTransaction_2.replace(R.id.container_body, fragment_2);
                        fragmentTransaction_2.addToBackStack(null);
                        fragmentTransaction_2.commit();
                        break;
                    case 2:
                        //Get fragment code
                        Fragment fragment_3 = new sukantu_vortacarj_3();

                        FragmentManager fragmentManager_3 = getFragmentManager();
                        FragmentTransaction fragmentTransaction_3 = fragmentManager_3.beginTransaction();
                        fragmentTransaction_3.replace(R.id.container_body, fragment_3);
                        fragmentTransaction_3.addToBackStack(null);
                        fragmentTransaction_3.commit();
                        break;

                    case 3:
                        //Get fragment code
                        Fragment fragment_4 = new sukantu_vortacarj_4();

                        FragmentManager fragmentManager_4 = getFragmentManager();
                        FragmentTransaction fragmentTransaction_4 = fragmentManager_4.beginTransaction();
                        fragmentTransaction_4.replace(R.id.container_body, fragment_4);
                        fragmentTransaction_4.addToBackStack(null);
                        fragmentTransaction_4.commit();
                        break;
                    case 4:
                        //Get fragment code
                        Fragment fragment_5 = new sukantu_vortacarj_5();

                        FragmentManager fragmentManager_5 = getFragmentManager();
                        FragmentTransaction fragmentTransaction_5 = fragmentManager_5.beginTransaction();
                        fragmentTransaction_5.replace(R.id.container_body, fragment_5);
                        fragmentTransaction_5.addToBackStack(null);
                        fragmentTransaction_5.commit();
                        break;
                    case 5:
                        //Get fragment code
                        Fragment fragment_6 = new sukantu_vortacarj_6();

                        FragmentManager fragmentManager_6 = getFragmentManager();
                        FragmentTransaction fragmentTransaction_6 = fragmentManager_6.beginTransaction();
                        fragmentTransaction_6.replace(R.id.container_body, fragment_6);
                        fragmentTransaction_6.addToBackStack(null);
                        fragmentTransaction_6.commit();
                        break;
                    case 6:
                        //Get fragment code
                        Fragment fragment_7 = new sukantu_vortacarj_7();

                        FragmentManager fragmentManager_7 = getFragmentManager();
                        FragmentTransaction fragmentTransaction_7 = fragmentManager_7.beginTransaction();
                        fragmentTransaction_7.replace(R.id.container_body, fragment_7);
                        fragmentTransaction_7.addToBackStack(null);
                        fragmentTransaction_7.commit();
                        break;
                    case 7:
                        //Get fragment code
                        Fragment fragment_8 = new sukantu_vortacarj_8();

                        FragmentManager fragmentManager_8 = getFragmentManager();
                        FragmentTransaction fragmentTransaction_8 = fragmentManager_8.beginTransaction();
                        fragmentTransaction_8.replace(R.id.container_body, fragment_8);
                        fragmentTransaction_8.addToBackStack(null);
                        fragmentTransaction_8.commit();
                        break;
                    case 8:
                        //Get fragment code
                        Fragment fragment_9 = new sukantu_vortacarj_9();

                        FragmentManager fragmentManager_9 = getFragmentManager();
                        FragmentTransaction fragmentTransaction_9 = fragmentManager_9.beginTransaction();
                        fragmentTransaction_9.replace(R.id.container_body, fragment_9);
                        fragmentTransaction_9.addToBackStack(null);
                        fragmentTransaction_9.commit();
                        break;
                    case 9:
                        //Get fragment code
                        Fragment fragment_10 = new sukantu_vortacarj_10();

                        FragmentManager fragmentManager_10 = getFragmentManager();
                        FragmentTransaction fragmentTransaction_10 = fragmentManager_10.beginTransaction();
                        fragmentTransaction_10.replace(R.id.container_body, fragment_10);
                        fragmentTransaction_10.addToBackStack(null);
                        fragmentTransaction_10.commit();
                        break;
                    case 10:
                        //Get fragment code
                        Fragment fragment_11 = new sukantu_vortacarj_11();

                        FragmentManager fragmentManager_11 = getFragmentManager();
                        FragmentTransaction fragmentTransaction_11 = fragmentManager_11.beginTransaction();
                        fragmentTransaction_11.replace(R.id.container_body, fragment_11);
                        fragmentTransaction_11.addToBackStack(null);
                        fragmentTransaction_11.commit();
                        break;
                    case 11:
                        //Get fragment code
                        Fragment fragment_12 = new sukantu_vortacarj_12();

                        FragmentManager fragmentManager_12 = getFragmentManager();
                        FragmentTransaction fragmentTransaction_12 = fragmentManager_12.beginTransaction();
                        fragmentTransaction_12.replace(R.id.container_body, fragment_12);
                        fragmentTransaction_12.addToBackStack(null);
                        fragmentTransaction_12.commit();
                        break;


                }


            }
        });


        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}