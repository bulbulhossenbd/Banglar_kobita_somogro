package bulbulhossen.banglakobita.Splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import bulbulhossen.banglakobita.Gridview_Main.Main_Activity;
import bulbulhossen.banglakobita.R;


/**
 * Created by bulbulkhan on 10/27/2015.
 */
public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        Thread myThread = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(1900);
                    Intent startMainscreen = new Intent(getApplicationContext(),Main_Activity.class);
                    startActivity(startMainscreen);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        myThread.start();
    }
}
