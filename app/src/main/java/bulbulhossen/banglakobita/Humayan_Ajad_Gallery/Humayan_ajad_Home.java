package bulbulhossen.banglakobita.Humayan_Ajad_Gallery;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import bulbulhossen.banglakobita.Gridview_Main.Main_Activity;
import bulbulhossen.banglakobita.R;


public class Humayan_ajad_Home extends AppCompatActivity implements Humayan_ajad_FragmentDrawer.FragmentDrawerListener {

    // decalre android lime toolbar object
    Toolbar androidlime_toolbar;
    private Humayan_ajad_FragmentDrawer drawerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //change code
        setContentView(R.layout.h_ajad_home);
        //change code

        // linking with xml
        androidlime_toolbar = (Toolbar) findViewById(R.id.toolbar);
        // giving actionbar support
        setSupportActionBar(androidlime_toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        //change code
        drawerFragment = (Humayan_ajad_FragmentDrawer)
                //change code
                getSupportFragmentManager().findFragmentById(R.id.h_ajad_navigation_drawer);
        //change code
        drawerFragment.setUp(R.id.h_ajad_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), androidlime_toolbar);
        //change code
        drawerFragment.setDrawerListener(this);

        if (savedInstanceState == null) {
            displayView(0);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {


            //Home page item
            case R.id.home_menu1:
                Intent home = new Intent(this, Main_Activity.class);
                startActivity(home);
                return true;

            //share apps item
            case R.id.share_menu2:

                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "My application name");
                    String share_comment = "\nLet me recommend you this application\n\n";
                    share_comment = share_comment + "https://play.google.com/store/apps/details?id=Orion.Soft \n\n";
                    i.putExtra(Intent.EXTRA_TEXT, share_comment);
                    startActivity(Intent.createChooser(i, "choose one"));
                } catch (Exception e) { //e.toString();
                }
                return true;


            //feedback

            case R.id.ovimot:
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                String[] recipients = new String[]{"yourmailaddress@email.com", "",};
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, recipients);
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Test");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "This is email's message");
                emailIntent.setType("text/plain");
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                return true;

            //apps rating item
            case R.id.rate_menu3:

                Uri uri = Uri.parse("market://details?id=" + this.getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=Orion.Soft" + this.getPackageName())));
                }

                return true;

            //develops by ..
            case R.id.about_menu4:
                final Dialog dialog1 = new Dialog(this); // Context, this, etc.
                dialog1.setContentView(R.layout.about_us);
                dialog1.setTitle(R.string.about);
                dialog1.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                fragment = new Humayan_ajad_Kobita_list();
                title = getString(R.string.humayun_ajad_kobita_list);
                break;

            case 1:
                fragment = new Humayan_ajad_1();
                title = getString(R.string.humayun_ajad_kobitar_name_1);
                break;

            case 2:
                fragment = new Humayan_ajad_2();
                title = getString(R.string.humayun_ajad_kobitar_name_2);
                break;


            case 3:
                fragment = new Humayan_ajad_3();
                title = getString(R.string.humayun_ajad_kobitar_name_3);
                break;

            case 4:
                fragment = new Humayan_ajad_4();
                title = getString(R.string.humayun_ajad_kobitar_name_4);
                break;


            case 5:
                fragment = new Humayan_ajad_5();
                title = getString(R.string.humayun_ajad_kobitar_name_5);
                break;


            case 6:
                fragment = new Humayan_ajad_6();
                title = getString(R.string.humayun_ajad_kobitar_name_6);
                break;

            case 7:
                fragment = new Humayan_ajad_7();
                title = getString(R.string.humayun_ajad_kobitar_name_7);
                break;

            case 8:
                fragment = new Humayan_ajad_8();
                title = getString(R.string.humayun_ajad_kobitar_name_8);
                break;

            case 9:
                fragment = new Humayan_ajad_9();
                title = getString(R.string.humayun_ajad_kobitar_name_9);
                break;

            case 10:
                fragment = new Humayan_ajad_10();
                title = getString(R.string.humayun_ajad_kobitar_name_10);
                break;
            case 11:
                fragment = new Humayan_ajad_11();
                title = getString(R.string.humayun_ajad_kobitar_name_11);
                break;

            case 12:
                fragment = new Humayan_ajad_12();
                title = getString(R.string.humayun_ajad_kobitar_name_12);
                break;


            default:
                fragment = new Humayan_ajad_Kobita_list();
                title = getString(R.string.humayun_ajad_kobita_list);
                break;

        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            fragmentTransaction.commit();

            // set the toolbar title
            getSupportActionBar().setTitle(title);
        }
    }
}
