package bulbulhossen.banglakobita.Gridview_Main;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;

import bulbulhossen.banglakobita.A_Hasan_Gallery.Hasan_Home;
import bulbulhossen.banglakobita.Adapter_Gallery.GridViewAdapter;
import bulbulhossen.banglakobita.Adapter_Gallery.ImageItem;
import bulbulhossen.banglakobita.Al_Mamud_gallery.Al_Mamud_Home;
import bulbulhossen.banglakobita.Humayan_Ajad_Gallery.Humayan_ajad_Home;
import bulbulhossen.banglakobita.Humayun_Ahmed_Gallery.Humayun_Home;
import bulbulhossen.banglakobita.Jibonnad_Das_Gallery.Jibonnad_Home;
import bulbulhossen.banglakobita.Joshim_Uddin_Gallery.Joshim_Home;
import bulbulhossen.banglakobita.Kazi_Nazrul_Islam.Kazi_Nazrul_Home;
import bulbulhossen.banglakobita.Rabinath_Tagore_Kobita_Gallery.Robinath_Tagore_Home;
import bulbulhossen.banglakobita.R;
import bulbulhossen.banglakobita.Shamsur_Gallery.Shamsur_Home;
import bulbulhossen.banglakobita.Sotnnath_dot_Gallery.Sotnnath_dot_Home;
import bulbulhossen.banglakobita.Sukantu_votacarj_Gallery.Sukantu_Home;
import bulbulhossen.banglakobita.Sukumar_Ray_Gallery.Sukumar_Home;


public class Main_Activity extends AppCompatActivity {

    // admob id
    private static final String AD_UNIT_ID = "ca-app-pub-2869508995487312/2690564381";
    private InterstitialAd interstitialAd;

    Intent i;
    private GridView gridView;
    private GridViewAdapter customGridAdapter;
    ArrayList<ImageItem> imageItems = new ArrayList<ImageItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gridview_main_activity);

        Bitmap one = BitmapFactory.decodeResource(this.getResources(), R.drawable.robi);
        Bitmap two = BitmapFactory.decodeResource(this.getResources(), R.drawable.kazi);
        Bitmap three = BitmapFactory.decodeResource(this.getResources(), R.drawable.joshim);
        Bitmap four = BitmapFactory.decodeResource(this.getResources(), R.drawable.shamsurrahman);
        Bitmap five = BitmapFactory.decodeResource(this.getResources(), R.drawable.ahsanhabib);
        Bitmap six = BitmapFactory.decodeResource(this.getResources(), R.drawable.humanahmed);
        Bitmap seven = BitmapFactory.decodeResource(this.getResources(), R.drawable.jibonnaddas);
        Bitmap eight = BitmapFactory.decodeResource(this.getResources(), R.drawable.sukumar);
        Bitmap nine = BitmapFactory.decodeResource(this.getResources(), R.drawable.sukantabhattacharya);
        Bitmap ten = BitmapFactory.decodeResource(this.getResources(), R.drawable.humayun_azad);
        Bitmap eleven = BitmapFactory.decodeResource(this.getResources(), R.drawable.sotonath);
        Bitmap twelve = BitmapFactory.decodeResource(this.getResources(), R.drawable.almamud);




        imageItems.add(new ImageItem(one, "রবীন্দ্রনাথ ঠাকুর"));
        imageItems.add(new ImageItem(two, "কাজী নজরুল ইসলাম"));
        imageItems.add(new ImageItem(three, "জসীমউদ্দীন"));
        imageItems.add(new ImageItem(four, "শামসুর রাহমান"));
        imageItems.add(new ImageItem(five, "আহসান হাবীব"));
        imageItems.add(new ImageItem(six, "হুমায়ূন আহমেদ"));
        imageItems.add(new ImageItem(seven, "জীবনানন্দ দাশ"));
        imageItems.add(new ImageItem(eight, "সুকুমার রায়"));
        imageItems.add(new ImageItem(nine, "সুকান্ত ভট্টাচার্য"));
        imageItems.add(new ImageItem(ten, "হুমায়ুন আজাদ"));
        imageItems.add(new ImageItem(eleven, "সত্যেন্দ্রনাথ দত্ত"));
        imageItems.add(new ImageItem(twelve, "আল মাহমুদ"));




        gridView = (GridView) findViewById(R.id.gridView);
        customGridAdapter = new GridViewAdapter(this, R.layout.row_grid, imageItems);
        gridView.setAdapter(customGridAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                switch (position) {
                    case 0:
                        i = new Intent(Main_Activity.this, Robinath_Tagore_Home.class);
                        break;
                    case 1:
                        i = new Intent(Main_Activity.this, Kazi_Nazrul_Home.class);
                        break;
                    case 2:
                        i = new Intent(Main_Activity.this, Joshim_Home.class);
                       // overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                        break;
                    case 3:
                        i = new Intent(Main_Activity.this, Shamsur_Home.class);
                        break;
                    case 4:
                        i = new Intent(Main_Activity.this, Hasan_Home.class);
                        break;
                    case 5:
                        i = new Intent(Main_Activity.this, Humayun_Home.class);
                        break;
                    case 6:
                        i = new Intent(Main_Activity.this, Jibonnad_Home.class);
                        break;
                    case 7:
                        i = new Intent(Main_Activity.this, Sukumar_Home.class);
                        break;
                    case 8:
                        i = new Intent(Main_Activity.this, Sukantu_Home.class);
                        break;

                    case 9:
                        i = new Intent(Main_Activity.this, Humayan_ajad_Home.class);
                        break;
                    case 10:
                        i = new Intent(Main_Activity.this, Sotnnath_dot_Home.class);
                        break;
                    case 11:
                        i = new Intent(Main_Activity.this, Al_Mamud_Home.class);
                        break;
                }

                startActivity(i);
            }

        });

    }


    //Application Exiting code

    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                Main_Activity.this);

        // set title
        alertDialogBuilder.setTitle("কবিতা ভাণ্ডার");

        // set dialog message
        alertDialogBuilder
                .setMessage("আপনি কি অ্যাপ্লিকেশান বন্ধ করতে চান ?")
                .setCancelable(false)
                .setPositiveButton("হ্যাঁ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        Main_Activity.this.finish();
                    }
                })
                .setNegativeButton("না", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();


//  Apps rating ... code start

/*        AppRate.with(this)
                //.setStoreType(AppRate.StoreType.GOOGLEPLAY) //default is Google, other option is Amazon
                .setInstallDays(3) // default 10, 0 means install day.
                .setLaunchTimes(10) // default 10 times.
                .setRemindInterval(2) // default 1 day.
                .setShowLaterButton(true) // default true.
                .setDebug(true) // default false.
                .setCancelable(false) // default false.
                .setOnClickButtonListener(new OnClickButtonListener() { // callback listener.
                    @Override
                    public void onClickButton(int which) {
                        Log.d(Main_Activity.class.getName(), Integer.toString(which));
                    }
                })
                .setMessage(R.string.meassage)
                .setTitle(R.string.new_rate_dialog_title)
                .setTextLater(R.string.new_rate_dialog_later)
                .setTextNever(R.string.new_rate_dialog_never)
                .setTextRateNow(R.string.new_rate_dialog_ok)
                .monitor();


        AppRate.showRateDialogIfMeetsConditions(this);*/

// rating code end


        //Admob start your apps than add show. ......code start

        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(AD_UNIT_ID);
        AdRequest adRequest = new AdRequest.Builder().build();
        interstitialAd.loadAd(adRequest);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {

                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                }

            }

            @Override
            public void onAdOpened() {
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {

            }
        });

        // admob code end


    }


}
